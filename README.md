## Hey 👋, I'm Erick Avila!


<a href="https://github.com/ErickAvilaB" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://linkedin.com/in/erickavilab" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>




### Glad to see you here!
Ayooooo! 🐶 I'm Erick, a computer science student at UNAM (Universidad Nacional Autónoma de México) with a passion for technology. I'm not just about code – I love diving into the worlds of literature, music, and cinema.


<br/>


## About Me
- 🎓 Studying Computer Science at UNAM


- 💻 Tech enthusiast with a focus on backend solutions


- 🧠 Love exploring the intersection of technology and humanities


<br/>


## Tech Toolbox
- 🐍 Python


- 🌐 JavaScript / TypeScript


- ⚙️ Arduino and Electronics


<br/>
<br/>


<div align="center"><b>Feel free to explore my repositories and join me on this tech journey! 🚀</b></div>


<br/>
